import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Todo } from '../models/todo';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  //URL del servidor con la API
  URL_API = 'http://localhost:4000/api/todos';

//creacion de los datos
  selectedTodo: Todo = {
    id: '',
    title: '',
    descript: '',
    completed: ''
  }
  todo: Todo[] = [];

  constructor( private http: HttpClient) { }

//Metodos para llamar nuestra API (CRUD)

//Traer listado
  getTodos(){
    return this.http.get<Todo[]>(this.URL_API);
  }

//Crear nuevo elemento
  createTodo(todo: Todo){
    return this.http.post(this.URL_API, todo);
  }

//Actualizar elemento
  updateTodo(todo: Todo){
    return this.http.put(this.URL_API+ '/' + todo.id, todo)
  }

//Borrar Elemento
  deleteTodo(id:string){
    return this.http.delete(this.URL_API+ '/' + id)
  }
}
