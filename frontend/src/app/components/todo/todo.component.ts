import { Component, OnInit } from '@angular/core';
import { TodoService } from '../../services/todo.service';
import { NgForm } from '@angular/forms';
import { Todo } from 'src/app/models/todo';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  constructor(public todoService: TodoService) { }

  ngOnInit(): void {
    this.getTodos();
  }

//Muestra el listado de los campos en la BD
  getTodos(){
    this.todoService.getTodos().subscribe(
      res => {
        this.todoService.todo = res;
      },
      err => console.error(err)
    )
  }

//Agregar o actualizar
  addTodo(form: NgForm){
    //se valida si en el formulario trae id
    //si es asi, editara
    if(form.value.id){
      this.todoService.updateTodo(form.value).subscribe(
        (res) => {
          console.log(res)
          form.reset();
          this.getTodos();
        },
        (err) => console.error(err)
      )
    //si no trae id, agregara  
    }else{
      this.todoService.createTodo(form.value).subscribe(
        res => {
          this.getTodos();
          form.reset();
        },
        err => console.error(err)
      )
    }
    
  }

//borrar dato
  deleteTodo(id:string){
    if(confirm('Are you sure you eant to delete it?')){
      this.todoService.deleteTodo(id).subscribe(
        (res) => {
          this.getTodos();
        },
        (err) => console.error(err)
      );
    }
  }

//editar campo
  editTodo(todo: Todo){
    this.todoService.selectedTodo = todo;
  }

//limpiar el formulario
  cleanForm(form: NgForm){
    form.reset();
    this.getTodos();
  }

//función para validar entrada de caracteres en el formulario
  inputValidator(event: any) {
    //console.log(event.target.value);
    const pattern = /^[a-zA-Z0-9]*$/;   
    //let inputChar = String.fromCharCode(event.charCode)
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^a-zA-Z0-9]/g, "");
      // invalid character, prevent input

    }
  }

}
