# Solo Challenge

I made this project with nodejs(express) and angular using a database in MySQL, as you can see, there is and file with extension .sql in this file we have the script to create the database that we need to this project.

Also we found two directories, the "server" that is the backend and the frontend, the versions that I use are Angular CLI 12.2.9 and
Node 16.11.1

# BACKEND
In the backend we can found the database.js configuration file and in the app.js there is the configuration for the server, we start the server with the command "npm run dev".
The backend is running in the port 4000

const mysqlConnection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'todos',
    insecureAuth : true
});

# FRONTEND
For the front part, it seems that we do not have to change any configuration since everything will run locally, only we need to start first the server and then the project frontend that is made in angular, so we start it with the command "ng serve -o" for open it in the browser.
The frontend is running in the port 4200 



