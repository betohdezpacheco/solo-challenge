const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const app = express();

//Se coloca el puerto de nuestro servidor
app.set('port', process.env.PORT || 4000);

//Asignamos el origen de donde nos pediran los datos por parte del front
app.use(cors({origin: "http://localhost:4200"}));
app.use(morgan('dev'));
//Leer datos JSON
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use("/api/todos", require('./routes/todos.routes'));

module.exports = app;