const { Router } = require('express');
const router = Router();

const todosController = require('../cnotrollers/todos.controller.js');

//Creacion de nuestras rutas
//GET ALL
router.get('/', todosController.getTodos);

//GET BY ID
router.get('/:id', todosController.getTodo);

//CREATE
router.post('/', todosController.createTodo);

//UPDATE
router.put('/:id', todosController.updateTodo);

//DELETE
router.delete('/:id', todosController.deleteTodo);

module.exports = router;