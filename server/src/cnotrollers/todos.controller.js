const todosController = {}

//require a nuestra conexion
const todosConnection = require('../database');

//Obtener todos los elementos
todosController.getTodos = (req, res) => {
    todosConnection.query('SELECT * FROM todos', (err, rows, fields) => {
        if(!err){
            res.json(rows);
        }else {
            console.log(err);
        }
    })
}

//Obtener un elemento por Id
todosController.getTodo = (req, res) => {
    const {id} = req.params;
    todosConnection.query('SELECT * FROM todos WHERE id = ?', [id], (err, rows, fields) => {
        if(!err){
            res.json(rows);
        }else {
            console.log(err);
        }
    })
}

//Crear un nuevo elemento
todosController.createTodo = (req, res) => {

    const { title, descript, completed } = req.body;

    todosConnection.query('INSERT INTO todos (`title`, `descript`, `completed`) VALUES (?, ?, ?)', [title, descript, completed], (err, rows, fields) => {
        if(!err){
            res.json({Status: 'Added'});
        }else {
            console.log(err);
        }
    })
}

//Actualizar elemento
todosController.updateTodo = (req, res) => {

    const { title, descript, completed } = req.body;
    const {id} = req.params;

    todosConnection.query('UPDATE todos SET `title` = ?, `descript` = ?, `completed` = ? WHERE `id` = ?', [title, descript, completed, id], (err, rows, fields) => {
        if(!err){
            res.json({Status: 'Updated'});
        }else {
            console.log(err);
        }
    })
}

//Borrar elemento
todosController.deleteTodo = (req, res) => {

    const {id} = req.params;
    todosConnection.query('DELETE FROM todos WHERE id = ?', [id], (err, rows, fields) => {
        if(!err){
            res.json({Status: 'Deleted'});
        }else {
            console.log(err);
        }
    })

}

module.exports = todosController;